# infra.ci.dagger-modules

> Reusable modules for [Dagger CI/CD][links.dagger]

<hr>
<sub>Emoji used for repository logo designed by <a href="https://openmoji.org/">OpenMoji</a> – the open-source emoji and icon project. License: CC BY-SA 4.0</sub>
<hr>

<!-- Links -->
[links.dagger]: https://dagger.io/
