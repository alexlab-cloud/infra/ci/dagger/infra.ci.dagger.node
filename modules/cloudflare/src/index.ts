/**
 * Reusable CI/CD functions for Cloudflare products
 */

import { func, object } from "@dagger.io/dagger"

@object()
class Cloudflare {
  @func()
  deployWorker(): WorkerDeploy {
    return new WorkerDeploy()
  }
}
