// src/index.ts
import { func, object } from "@dagger.io/dagger"

@object()
export class WorkerDeploy {
    @func()
    deploy
}