/**
 * Reusable CI/CD functions for general tasks
 */

import { func, object } from "@dagger.io/dagger"

import { WorkerDeploy } from "./projects"

@object()
class Utils {
  @func()
  summarizeProjects(): WorkerDeploy {
    return new ProjectsSummary()
  }
}
